export interface RootStateModel {
    user: UserStateModel;
    setting: SettingStateModel;
  }

export interface UserStateModel {
  uuid: string;
  name: string;
  login: string;
}

export interface SettingStateModel {
  color: string;
  language: string;
}