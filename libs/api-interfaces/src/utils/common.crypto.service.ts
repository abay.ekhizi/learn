import Rijndael from 'rijndael-js';

export class CommonCryptoService {
  private passBytes: any;
  static FIELD_KEY = '__';

  constructor(encryptionKey: string) {
    //@ts-ignore
    this.passBytes = Buffer.from(encryptionKey, 'utf8');
  }

  encryptData(value: string) {
    try {
      let EncryptionkeyBytes = new Array(16).fill(0x00);
      let len = this.passBytes.length;
      if (len > EncryptionkeyBytes.length) {
        len = EncryptionkeyBytes.length;
      }
      EncryptionkeyBytes = this.passBytes.slice(0, len);
      const key = EncryptionkeyBytes;
      const iv = EncryptionkeyBytes;

      //@ts-ignore
      const cipher = new Rijndael(key, 'cbc');
      //@ts-ignore
      const ciphertext = Buffer.from(cipher.encrypt(value, 128, iv));

      const res = ciphertext.toString('base64');
      return res;
    } catch (error) {
      console.error(value);
      throw error;
    }
  }

  decryptData(value: string) {
    try {
      let EncryptionkeyBytes = new Array(16).fill(0x00);
      let len = this.passBytes.length;
      if (len > EncryptionkeyBytes.length) {
        len = EncryptionkeyBytes.length;
      }
      EncryptionkeyBytes = this.passBytes.slice(0, len);
      const key = EncryptionkeyBytes;
      const iv = EncryptionkeyBytes;

      //@ts-ignore
      const cipher = new Rijndael(key, 'cbc');
      //@ts-ignore
      const cipherBuffer = Buffer.from(value, 'base64');
      //@ts-ignore
      const plaintext = Buffer.from(cipher.decrypt(cipherBuffer, 128, iv))
        .toString()
        .trim();

      const res = plaintext.replace(/\0.*$/g, ''); //Remove the null byte
      return res;
    } catch (error) {
      console.error(value);
      throw error;
    }
  }

  encryptBody(body: null | object) {
    if (body == null) return null;
    if (typeof body !== 'object') {
      return body != null ? this.encryptData(String(body)) : body;
    }
    const newBody: Record<string, any> = {};
    for (const [key, value] of Object.entries(body)) {
      let res;
      if (typeof value === 'object' && !Array.isArray(value)) {
        res = this.encryptBody(value);
      } else if (typeof value === 'object' && Array.isArray(value)) {
        res = value.map(v => this.encryptBody(v));
      } else {
        res = value != null ? this.encryptData(value.toString()) : value;
      }
      newBody[this.encryptData(key)] = res;
    }
    return newBody;
  }

  decryptBody(body: null | object) {
    if (body == null) return body;
    if (typeof body !== 'object') {
      return body != null ? this.decryptBodyField(String(body)) : body;
    }
    const newBody: Record<string, any> = {};
    for (const [key, value] of Object.entries(body)) {
      let res;
      if (typeof value === 'object' && !Array.isArray(value)) {
        res = this.decryptBody(value);
      } else if (typeof value === 'object' && Array.isArray(value)) {
        res = value.map(v => this.decryptBody(v));
      } else {
        res = value != null ? this.decryptBodyField(value.toString()) : value;
      }
      newBody[this.decryptData(key)] = res;
    }
    return newBody;
  }

  decryptBodyField(value: any) {
    const res = this.decryptData(value);
    return res
      ? res === 'true'
        ? true
        : res === 'false'
        ? false
        : !isNaN(Number(res))
        ? Number(res)
        : res
      : res;
  }
}
