export interface Message {
  message: string;
}

export type ApiGateway = 'node' | '.net' 