import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { RootStateModel } from "@learning/api-interfaces";
import { SelectSnapshot } from "@ngxs-labs/select-snapshot";
import { Observable } from "rxjs";
import { CryptoService } from "./crypto";

@Injectable({
    providedIn : "root"
})
export class RequestInterceptor implements HttpInterceptor{
    @SelectSnapshot((state : RootStateModel) => state.user.uuid)
    uuid : string

    constructor(private cryptoService : CryptoService){
    }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
       
        const clonedRequest = req.clone({
            body : this.cryptoService.encryptBody(req.body),
            headers : req.headers.append('uuid', this.uuid)
        })

        return next.handle(clonedRequest)
    }

}