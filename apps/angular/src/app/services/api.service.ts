import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

export type ApiGateway = 'node' | '.net'

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  makeUrl(customUrl: string, gateway : ApiGateway) {
    const baseUrl =
      gateway == 'node'
        ? environment.NODE_SERVER
        : environment.NET_SERVER;
      return baseUrl +'/'+ customUrl
  }

  post<Body = any, Return = any>(url: string, body: any, gateway: ApiGateway = 'node') {
        return this.http.post<Return>(this.makeUrl(url,gateway),body)
        }
}
