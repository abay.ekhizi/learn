import { Injectable } from "@angular/core";
import { CommonCryptoService } from "@learning/api-interfaces";
import { environment } from "../../environments/environment";

@Injectable({
    providedIn : "root"
})
export class CryptoService extends CommonCryptoService{
    constructor(){
        super(environment.ENCRYPTION_KEY)
    }
}