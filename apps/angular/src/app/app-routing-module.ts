import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import {LoginGuardService} from './services/login-guard.service'

const routes : Routes = [
  {path : '', redirectTo : 'login', pathMatch : 'full'},
  {path : 'login' , component : LoginComponent, canActivate : [LoginGuardService]}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
