import { Injectable } from '@angular/core';
import { SettingStateModel } from '@learning/api-interfaces';
import {
  State,
  NgxsOnChanges,
  NgxsSimpleChange,
  Action,
  StateContext,
} from '@ngxs/store';
import { UpdateColor } from '../action/setting.action';

@State<SettingStateModel>({
  name: 'setting',
  defaults: {
    color: '',
    language: '',
  },
})
@Injectable()
export class SettingState implements NgxsOnChanges {
  ngxsOnChanges(change: NgxsSimpleChange<any>): void {}

  @Action(UpdateColor)
  UpdateColor(ctx: StateContext<SettingStateModel>, payload: UpdateColor) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      color: payload.color,
    });
  }
}
