import { Injectable } from '@angular/core';
import { UserStateModel } from '@learning/api-interfaces';
import { State, NgxsOnChanges, NgxsSimpleChange, Action, StateContext } from '@ngxs/store';
import { v4 as uuidv4 } from 'uuid';
import { UpdateUserLoginAndName } from '../action/user.action';

@State<UserStateModel>({
  name: 'user',
  defaults: {
    uuid: uuidv4(),
    name: '',
    login: '',
  },
})
@Injectable()
export class UserState implements NgxsOnChanges {

  ngxsOnChanges(change: NgxsSimpleChange<any>): void { }
@Action(UpdateUserLoginAndName)
  UpdateUserLoginAndName(ctx : StateContext<UserStateModel>, payload : UpdateUserLoginAndName ){
      const state = ctx.getState()
      ctx.setState({
          ...state,
          login : payload.login,
          name : payload.name
      })
  }
}
