export class UpdateColor {
  static readonly type = '[setting] update color setting';
  constructor(public color: string) {}
}
