export class UpdateUserLoginAndName{
    static readonly type ='[User] update user login and name'
    constructor(public name : string, public login : string){}
}