import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { environment } from '../../environments/environment';
import {ApiService} from '../services/api.service'
import { UpdateUserLoginAndName } from '../store/action/user.action';
import { SelectSnapshot } from '@ngxs-labs/select-snapshot';
import { RootStateModel } from '@learning/api-interfaces';
import { Observable } from 'rxjs';

@Component({
  selector: 'learning-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @SelectSnapshot((state : RootStateModel)=> state.user.name)
  name : string

  @SelectSnapshot((state : RootStateModel) => state.user.uuid)
  uuid : string

  @Select((state : RootStateModel)=> state.user.name)
  name$ : Observable<String>

  constructor(private apiService : ApiService , private store : Store) { }
  envName = environment.NAME
  display = true
  login : string = ''
  password : string = ''

  ngOnInit(): void {}

  async verification(){
    const payload = {
      login : this.login,
      password : this.password
    }
    this.store.dispatch(new UpdateUserLoginAndName('abay','aekhizi'))
    console.log('this.uuid :>> ', this.uuid);
    // console.log('ENV NAME :>> ', environment.NAME);
    // const authResponse = await this.apiService.post('authentification', payload).toPromise()
    // console.log('authResponse :>> ', authResponse);

  }

}
