export const environment = {
    production: false,
    NAME: '(TEST)',
    NET_SERVER: 'https://localhost:44389',
    NODE_SERVER: 'http://localhost:3333',
    SOCKET_SERVER: 'http://localhost:3333',
    SOCKET_PATH: '/socket.io',
    ENCRYPTION_KEY : 'abay'
  };
  