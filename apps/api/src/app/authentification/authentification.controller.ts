import { Controller, Get, Post } from '@nestjs/common';
import { AuthentificationService } from './authentification.service';

@Controller('authentification')
export class AuthentificationController {
    constructor(private service : AuthentificationService){}

    @Post()
    userAuthentification(){
        return this.service.userAuth()
    }

    @Get()
    userAuthentifications(){
        return 'abay'
    }

}
