import { Module } from '@nestjs/common';
import { AuthentificationController } from './authentification.controller';
import { AuthentificationService } from './authentification.service';

@Module({
    providers : [AuthentificationService],
    controllers : [AuthentificationController]
})
export class AuthentificationModule {}
