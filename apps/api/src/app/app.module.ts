import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthentificationController } from './authentification/authentification.controller';
import { AuthentificationModule } from './authentification/authentification.module';

@Module({
  imports : [AuthentificationModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
